<?php

/**
 * Template Name: Gallery - Basic
 */

get_header();

?>


<div id="content" class="page">

  <div class="page-intro">
    <?php if( get_field('cover_image_gallery') ): ?>
      <div class="intro-inner" style="background-image: url('<?php the_field('cover_image_gallery'); ?>')">
	  <?php endif; ?>
      <div class="outer-inner">
        <div class="inner-box clearfix">
          <div class="inner-box-container">
            <div class="intro-title">
              <h2><?php the_title(); ?> </h2>
              <p>Connecting outstanding people.</p>
            </div>
          </div><!-- end .inner-container -->
        </div><!-- end .inner-box -->
      </div><!-- end .outer-inner -->
    </div><!-- end .intro-inner -->
  </div><!-- end .page-intro -->

  <div class="content-wrap">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <?php

            $images = get_field('images_gallery');
            $size = 'full'; // (thumbnail, medium, large, full or custom size)

            if( $images ): ?>
              <div class="row">
                <?php foreach( $images as $image ): ?>
                    <div class="col-md-4 col-sm-4 col-xs-12">
                      <a href="<?php echo $image['sizes']['large']; ?>" data-fancybox="gallery" class="fancybox img-<?php echo $counter; ?>" rel="mini">
                        <img class="img-responsive" src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['title']; ?>" />
                      </a>
                    </div>
                <?php endforeach; ?>
              </div>
            <?php endif; ?>

        </div><!-- end .col-md-12 col-sm-12 col-xs-12 -->
      </div><!-- end .row -->
    </div><!-- end .container -->
  </div><!-- end .content-wrap -->

</div><!-- end #content -->

<?php get_footer(); ?>
