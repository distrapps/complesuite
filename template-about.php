<?php

/**
 * Template Name: About
 */

get_header();

?>


<div id="content" class="page">

  <div class="page-intro">
    <?php if( get_field('cover_image_about') ): ?>
      <div class="intro-inner" style="background-image: url('<?php the_field('cover_image_about'); ?>')">
	  <?php endif; ?>
      <div class="outer-inner">
        <div class="inner-box clearfix">
          <div class="inner-box-container">
            <div class="intro-title">
              <h2><?php the_title(); ?> </h2>
              <p>Connecting outstanding people.</p>
            </div>
          </div><!-- end .inner-container -->
        </div><!-- end .inner-box -->
      </div><!-- end .outer-inner -->
    </div><!-- end .intro-inner -->
  </div><!-- end .page-intro -->

  <div class="content-wrap">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <?php the_field('pa_description'); ?>
        </div>
      </div>
    </div>
  </div><!-- end .content-wrap -->


</div>


<?php get_footer(); ?>
