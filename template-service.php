<?php

/**
 * Template Name: Service
 */

get_header();

?>

<style media="all">
  .box-odd .dimg {
    float: right;
  }
  .box-odd .dtxt {
    float: left;
  }
</style>

<div id="content" class="page">

  <div class="page-intro">
    <?php if( get_field('page_service_cover') ): ?>
      <div class="intro-inner" style="background-image: url('<?php the_field('page_service_cover'); ?>')">
	  <?php endif; ?>
      <div class="outer-inner">
        <div class="inner-box clearfix">
          <div class="inner-box-container">
            <div class="intro-title">
              <h2><?php the_title(); ?> </h2>
              <p>Simply elegance</p>
            </div>
          </div><!-- end .inner-container -->
        </div><!-- end .inner-box -->
      </div><!-- end .outer-inner -->
    </div><!-- end .intro-inner -->
  </div><!-- end .page-intro -->

  <div class="content-wrap">
    <?php

      // check if the repeater field has rows of data
      if( have_rows('sf_content') ):
        $i = 1;
        // loop through the rows of data
        while ( have_rows('sf_content') ) : the_row(); ?>
          <section class="section-servicebox section-padspace <?php echo $i % 2 == 0 ? 'box-odd' : ''; ?>" data-aos="fade-up" data-aos-duration="1000">
            <div class="container">
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 dimg">
                  <div class="box-content service-post">
                    <div class="box-image">
                      <span>
                        <img src="<?php the_sub_field('sf_image'); ?>" alt="">
                      </span>
                    </div><!-- end .box-image -->
                  </div><!-- end .box-content -->
                </div><!-- end .col-md-4 col-sm-4 col-xs-4 -->
                <div class="col-md-8 col-sm-8 col-xs-12 dtxt">
                  <div class="describox">
                    <div class="description-post">
                      <h3><?php the_sub_field('sf_title'); ?></h3>
                      <?php the_sub_field('sf_text'); ?>
                    </div><!-- end .description-post -->
                  </div><!-- end .describox -->
                </div><!-- end .col-md-4 col-sm-4 col-xs-4 -->
              </div><!-- end .row -->
            </div><!-- end .container -->
          </section>
    <?php
    $i++;
      endwhile;

    else :

      // no rows found

    endif;

  ?>

  </div><!-- end .content-wrap -->
</div>


<?php get_footer(); ?>
