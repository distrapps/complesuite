<?php

// -----------------------------------------------------------------------------
// Main Menu / Navwalker
// -----------------------------------------------------------------------------
require_once get_template_directory() . '/wp-bootstrap-navwalker.php';
register_nav_menus(array(
	'primary' => __( 'Primary Menu', 'header-menu' ),
));

function mytheme_add_woocommerce_support(){
  add_theme_support('woocommerce', array(
  'thumbnail_image_width' => 150,
  'single_image_width' => 300,
  'product_grid' => array(
    'default_rows' => 3,
    'min_rows' => 2,
    'max_rows' => 8,
    'default_columns' => 4,
    'min_columns' => 2,
    'max_columns' => 5,
    ),
  ));
}
add_action('after_setup_theme', 'mytheme_add_woocommerce_support');

// -----------------------------------------------------------------------------
// woocommerce zoom
// -----------------------------------------------------------------------------
function mytheme_add_woocommerce_zooming(){
  add_theme_support('wc-product-gallery-zoom');
  //add_theme_support('wc-product-gallery-lightbox');
  //add_theme_support('wc-product-gallery-slider');
}
add_action('wp', 'mytheme_add_woocommerce_zooming', 99);

function cl (){
    the_excerpt();
}
add_action('woocommerce_after_shop_lopp_item_title', 'cl', 40);

// -----------------------------------------------------------------------------
// ACF Option page
// -----------------------------------------------------------------------------
if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Theme Service Facilities',
		'menu_title'	=> 'Service & Facilities',
		'menu_slug' 	=> 'theme-service-settings',
		'capability'	=> 'edit_posts',
		'icon_url' => 'dashicons-feedback',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Rooms',
		'menu_title'	=> 'Rooms',
		'menu_slug' 	=> 'theme-rooms-settings',
		'capability'	=> 'edit_posts',
		'icon_url' => 'dashicons-store',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Testimonial',
		'menu_title'	=> 'Testimonial',
		'menu_slug' 	=> 'theme-testimonial-settings',
		'capability'	=> 'edit_posts',
		'icon_url' => 'dashicons-store',
		'redirect'		=> false
	));
}

// -----------------------------------------------------------------------------
// Other / Related Blog Post
// -----------------------------------------------------------------------------
function example_cats_related_post() {
  $post_id = get_the_ID();
  $cat_ids = array();
  $categories = get_the_category( $post_id );

  if(!empty($categories) && is_wp_error($categories)):
    foreach ($categories as $category):
        array_push($cat_ids, $category->term_id);
    endforeach;
  endif;

  $current_post_type = get_post_type($post_id);
  $query_args = array(
      'category__in'   => $cat_ids,
      'post_type'      => $current_post_type,
      'post_not_in'    => array($post_id),
      'posts_per_page'  => '3'
 	);

  $related_cats_post = new WP_Query( $query_args );

	if($related_cats_post->have_posts()):
		while($related_cats_post->have_posts()): $related_cats_post->the_post(); ?>
		  <div class="col-md-4 col-sm-4 col-xs-12">
				<div class="box-image">
					<span>
						<?php
							if ( has_post_thumbnail() ) {
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
								echo '<img src="'.$image[0].'" data-id="'.$post->ID.'" class="img-responsive">';
							}
						?>
					</span>
				</div><!-- end .box-image -->
	      <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				<span>Post by <?php the_author(); ?>, <?php the_time('F jS, Y'); ?></span>
	      <?php the_content(); ?>
		  </div>
		<?php endwhile;

		// Restore original Post Data
		wp_reset_postdata();
	endif;
}


add_action('acf/render_field_settings/type=image', 'add_default_value_to_image_field', 20);
function add_default_value_to_image_field($field) {
  $args = array(
    'label' => 'Default Image',
    'instructions' => 'Appears when creating a new post',
    'type' => 'image',
    'name' => 'default_value'
  );
  acf_render_field_setting($field, $args);
}

add_action('admin_enqueue_scripts', 'enqueue_uploader_for_image_default');
function enqueue_uploader_for_image_default() {
  $screen = get_current_screen();
  if ($screen && $screen->id && ($screen->id == 'acf-field-group')) {
    acf_enqueue_uploader();
  }
}

add_filter('acf/load_value/type=image', 'reset_default_image', 10, 3);

function reset_default_image($value, $post_id, $field) {
  if (!$value) {
    $value = $field['default_value'];
  }
  return $value;
}

// loading the field
add_action('acf/load_field/name=SF_uImage', 'load_select_field_name_choices');
function load_select_field_name_choices($field) {
  $choices = array();
  if (have_rows('sf_uno', 'options')) {
    while (have_rows('sf_uno', 'options')) {
      the_row();
      $title = get_sub_field('SF_uImage');
      $choices[$title] = $title;
    } // end while have rows
  }  // end if get field
} // end function

// -----------------------------------------------------------------------------
// Main Image Size Setting
// -----------------------------------------------------------------------------
if (function_exists('add_theme_support')) {
    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true);
    add_image_size('medium', 320, 200, true);
    add_image_size('small', 120, '', true);
    add_image_size('full');
    add_image_size('admin-list-thumb', 80, 80, true);
    add_image_size('album-grid', 450, 450, true );
    add_image_size('gallery-slide', 900, 500, true);
    add_image_size('custom-size', 900, 300, true);
    add_image_size('gallery-slide-main', 1920, 1080, true);
}

// -----------------------------------------------------------------------------
// Style and vendor
// -----------------------------------------------------------------------------
function my_theme_enqueue_styles() {

    $parent_style = 'complesuite-style';
    wp_enqueue_script('jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.3/jquery.min.js', array(), false, true );
		wp_enqueue_script('jsbootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array(), false, true );
    wp_enqueue_script('jsflexslider', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/jquery.flexslider.js', array(), false, true );
    wp_enqueue_script('jsaos', 'https://unpkg.com/aos@2.3.1/dist/aos.js', array(), false, true );
		wp_enqueue_script('jsowl', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', array(), false, true );
		wp_enqueue_script('jsfancybox', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js', array(), false, true );
    wp_enqueue_script('jsvendor', get_stylesheet_directory_uri() .  '/asset/js/vendor/vendor.min.js', array(), false, true );
    wp_enqueue_script('jsglobal', get_stylesheet_directory_uri() .  '/asset/js/global.js', array(), false, true );

    wp_enqueue_style('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), '3.3.7','all' );
    wp_enqueue_style('aos', 'https://unpkg.com/aos@2.3.1/dist/aos.css', array(), '2.3.1','all' );
    wp_enqueue_style('font-awesome', 'https://use.fontawesome.com/releases/v5.0.4/css/all.css', array(), '5.0.4','all' );
    wp_enqueue_style('flexslider', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.1/flexslider.min.css', array(), '2.7.1','all' );
		wp_enqueue_style('owlcarousel', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css', array(), '2.3.4','all' );
		wp_enqueue_style('fancybox', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css', array(), '2.3.4','all' );
    wp_enqueue_style('fontello', get_stylesheet_directory_uri() . '/asset/fontello/css/marker.css', array(), '2.7.1','all' );
		wp_enqueue_style('themify', get_stylesheet_directory_uri() . '/asset/fonts/themify-icons.css', array(), '2.7.1','all' );
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );

}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles', 60 );

// -----------------------------------------------------------------------------
// Map
// -----------------------------------------------------------------------------
function my_acf_init() {
	// acf_update_setting('google_api_key', 'AIzaSyBtTTehYG6_aGzRNpXMWSV4Q952Gd5hh5Y');
	acf_update_setting('google_api_key', 'AIzaSyDmvXT3dnOfHS52c5vcBSf9kyOC_zQqVuA');
}

add_action('acf/init', 'my_acf_init');
// define( 'google_api_key', 'AIzaSyBtTTehYG6_aGzRNpXMWSV4Q952Gd5hh5Y' );

function acf_map(){ ?>
	<style type="text/css">
		.acf-map {
			width: 100%;
			height: 350px;
			margin: 20px 0 0;
		}
		/* fixes potential theme css conflict */

		.acf-map img {
			max-width: inherit !important;
		}
	</style>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDmvXT3dnOfHS52c5vcBSf9kyOC_zQqVuA"></script>
	<script type="text/javascript">
		(function ($) {

			/*
			 *  new_map
			 *
			 *  This function will render a Google Map onto the selected jQuery element
			 *
			 *  @type	function
			 *  @date	8/11/2013
			 *  @since	4.3.0
			 *
			 *  @param	$el (jQuery element)
			 *  @return	n/a
			 */

			function new_map($el) {

				// var
				var $markers = $el.find('.marker');


				// vars
				var args = {
					zoom: 20,
					center: new google.maps.LatLng(0, 0),
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};


				// create map
				var map = new google.maps.Map($el[0], args);


				// add a markers reference
				map.markers = [];


				// add markers
				$markers.each(function () {

					add_marker($(this), map);

				});


				// center map
				center_map(map);


				// return
				return map;

			}

			/*
			 *  add_marker
			 *
			 *  This function will add a marker to the selected Google Map
			 *
			 *  @type	function
			 *  @date	8/11/2013
			 *  @since	4.3.0
			 *
			 *  @param	$marker (jQuery element)
			 *  @param	map (Google Map object)
			 *  @return	n/a
			 */

			function add_marker($marker, map) {

				// var
				var latlng = new google.maps.LatLng($marker.attr('data-lat'), $marker.attr('data-lng'));

				// create marker
				var marker = new google.maps.Marker({
					position: latlng,
					map: map
				});

				// add to array
				map.markers.push(marker);

				// if marker contains HTML, add it to an infoWindow
				if ($marker.html()) {
					// create info window
					var infowindow = new google.maps.InfoWindow({
						content: $marker.html()
					});

					// show info window when marker is clicked
					google.maps.event.addListener(marker, 'click', function () {

						infowindow.open(map, marker);

					});
				}

			}

			/*
			 *  center_map
			 *
			 *  This function will center the map, showing all markers attached to this map
			 *
			 *  @type	function
			 *  @date	8/11/2013
			 *  @since	4.3.0
			 *
			 *  @param	map (Google Map object)
			 *  @return	n/a
			 */

			function center_map(map) {

				// vars
				var bounds = new google.maps.LatLngBounds();

				// loop through all markers and create bounds
				$.each(map.markers, function (i, marker) {

					var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());

					bounds.extend(latlng);

				});

				// only 1 marker?
				if (map.markers.length == 1) {
					// set center of map
					map.setCenter(bounds.getCenter());
					map.setZoom(18);
				} else {
					// fit to bounds
					map.fitBounds(bounds);
				}

			}

			/*
			 *  document ready
			 *
			 *  This function will render each map when the document is ready (page has loaded)
			 *
			 *  @type	function
			 *  @date	8/11/2013
			 *  @since	5.0.0
			 *
			 *  @param	n/a
			 *  @return	n/a
			 */
			// global var
			var map = null;

			$(document).ready(function () {

				$('.acf-map').each(function () {

					// create map
					map = new_map($(this));

				});

			});

		})(jQuery);
	</script>
	<?php }
add_action('wp_footer', 'acf_map');

//breadcrumbs
if ( ! function_exists( 'breadcrumbs' ) ) :
function breadcrumbs() {
$delimiter = '&rsaquo;';
$home = 'Home';

echo '<div xmlns:v="http://rdf.data-vocabulary.org/#">';
global $post;
echo ' <span typeof="v:Breadcrumb">
<a rel="v:url" property="v:title" href="'.home_url( '/' ).'">'.$home.'</a>
</span> ';
$cats = get_the_category();
if ($cats) {
foreach($cats as $cat) {
echo $delimiter . "<span typeof=\"v:Breadcrumb\">
<a rel=\"v:url\" property=\"v:title\" href=\"".get_category_link($cat->term_id)."\" >$cat->name</a>
</span>"; }
}
echo $delimiter . the_title(' <span>', '</span>', false);
echo '</div>';
}
endif;

function torque_breadcrumbs() {
	/* Change according to your needs */
	$show_on_homepage = 0;
	$show_current = 1;
	$delimiter = '&raquo;';
	$home_url = 'Home';
	$before_wrap = '<span clas="current">';
	$after_wrap = '</span>';

	/* Don't change values below */
	global $post;
	$home_url = get_bloginfo( 'url' );

	/* Check for homepage first! */
	if ( is_home() || is_front_page() ) {
		$on_homepage = 1;
	}
	if ( 0 === $show_on_homepage && 1 === $on_homepage ) return;

	/* Proceed with showing the breadcrumbs */
	$breadcrumbs = '<ol id="crumbs" itemscope itemtype="http://schema.org/BreadcrumbList">';

	$breadcrumbs .= '<li itemprop="itemListElement" itemtype="http://schema.org/ListItem"><a target="_blank" href="' . $home_url . '">' . $home_url . '</a></li>';

	/* Build breadcrumbs here */

	$breadcrumbs .= '</ol>';

	echo $breadcrumbs;
}

// -----------------------------------------------------------------------------
// Update Checker
// -----------------------------------------------------------------------------
require_once ( get_stylesheet_directory() . '/inc/plugin-update-checker/plugin-update-checker.php' );
    $updateChecker = Puc_v4_Factory::buildUpdateChecker(
        'https://bitbucket.org/distrapps/complesuite',
        __FILE__,
        'complesuite'
    );

    $updateChecker->setAuthentication( array(
        'consumer_key' => 'fgRqxkNVeWkCxpumeT',
        'consumer_secret' => 'eJJTb6YYSGjVKZ6LszVgrGPejR79BKH8',
    ));

    $updateChecker->setBranch( 'master' );

?>
