=== Complement Suite Template ===
Contributors: Distrapps
Tags: simple, company profile, hotel, resort
Requires at least: 4.8
Stable tag: v1.0.2
Version: 1.0.2
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html


== Description ==
Complement Suite (CompleSuite) Template


== Installation ==
This section describes how to install the child theme and get it working.

== Changelog ==

= 1.0.2 =
* tidy up components and section  

= 1.0.1 =
* setup main components and section

= 1.0.0 =
* initial files
