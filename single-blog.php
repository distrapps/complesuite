<?php

/*
 * Template Name: Featured Article
 * Template Post Type: post, page, product
 */

get_header();

?>

<div id="content" class="page">

  <div class="page-intro">
    <?php
      $image = get_field('page_blog_detail_cover', 'option');
      if( !empty($image) ): ?>
          <div class="intro-inner" style="background-image: url('<?php echo $image['url']; ?>')">
      <?php endif; ?>
      <div class="outer-inner">
        <div class="inner-box clearfix">
          <div class="inner-box-container">
            <div class="intro-title">
              <h2>Blog</h2>
              <p>Connecting outstanding people.</p>
            </div>
          </div><!-- end .inner-container -->
        </div><!-- end .inner-box -->
      </div><!-- end .outer-inner -->
    </div><!-- end .intro-inner -->
  </div><!-- end .page-intro -->

  <div class="content-wrap">
    <div class="container">
      <div class="row">
        <?php
          if (have_posts()): while (have_posts()) : the_post(); ?>
          <div id="breadcrumbs">
          <?php breadcrumbs(); ?>
          </div>
          <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="box-list">
              <div class="headingblog">
                <h1><?php the_title();?></h1>
                <div class="meta">
                  <span class="author"><i class="ti-user"></i><a href="#">by <?php the_author(); ?></a></span>
                  <span class="date"><i class="ti-calendar"></i><a href="#"> <?php the_time('F jS, Y'); ?></a></span>
                </div>
              </div>
              <div class="imgblog">
                <?php the_post_thumbnail( 'gallery-slide-main', array('class' => 'img-responsive') );?>
              </div>



              <div class="contentblog">
                <?php the_content(); ?>
              </div>
            </div>
          </div><!-- end .col-md-8 -->

        <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>

          <div class="col-md-4 col-sm-4">
            <aside class="sidebar sidebar-right">
              <div class="widget">
                <div class="widget blog-heading blog-category">
                  <h3 class="widget-title">Blog Category</h3>
                  <?php
                    $args = array('pad_counts' => true, 'get' => 'all');
                    $cats = get_terms('category', $args);
                  ?>
                  <ul>
                    <?php foreach( $cats as $category ) : ?>
                  	   <li><a href="<?php echo get_category_link( $category->term_id ); ?>"><?php echo $category->name; ?></a>
                  	      (<?php echo $category->count; ?>)
                        </li>
                    	<?php unset( $category ); ?>
                      <?php endforeach;
                      unset( $cats );
                    ?>
                  </ul>
                </div><!-- end .widget -->
                <div class="widget blog-heading blog-recent-post">
                  <h3 class="widget-title">Recent Post</h3>
                  <ul>
                    <?php
                      $args = array( 'posts_per_page' => '3' );
                      $recent_posts = new WP_Query($args);
                      while( $recent_posts->have_posts() ) :
                          $recent_posts->the_post() ?>
                          <li>
                              <a href="<?php echo get_permalink() ?>"><?php the_title() ?></a>
                              <?php if ( has_post_thumbnail() ) : ?>
                                  <?php the_post_thumbnail('thumbnail') ?>
                              <?php endif ?>
                              <?php echo '<span class="comment">' . date_i18n('d F Y', strtotime($recent['post_date'])) .'</span> '; ?>
                              <?php echo get_the_category_list( ', ', '', $recent["ID"] ); ?>
                          </li>
                      <?php endwhile; ?>
                      <?php wp_reset_postdata(); # reset post data so that other queries/loops work
                    ?>
                  </ul>
                </div><!-- end .widget -->
              </div><!-- end .widget -->
            </aside>
          </div><!-- end .col-md-4 -->
      </div><!-- end .row -->
    </div><!-- end .container -->
  </div><!-- end .content-wrap -->

  <div class="container">
    <div class="row">
      <div class="blogrelated">
          <h4>Other Blog</h4>
          <?php example_cats_related_post() ?>
      </div><!-- end .blogrelated -->
    </div><!-- end .row -->
  </div><!-- end .container -->
</div><!-- end #content -->

<?php get_footer(); ?>
