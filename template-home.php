<?php

/**
 * Template Name: Homepage
 */

get_header();

?>

<style media="all">
  .flexi .slides > li {
    padding-bottom: 60%!important;
  }

  body p {
    color: <?php the_field('main_color', 'options'); ?>;
  }
</style>

<div id="content" class="homepage">
  <div class="section slider-home">
    <?php
      $images = get_field('home_slider');
      if( $images ): ?>

        <div id="slider-main" class="owl-carousel">
          <?php foreach( $images as $image ): ?>
            <div class="owl-slide">
              <div class="image-slide" style="background-image: url('<?php echo $image['url']; ?>')"></div>
              <div class="container">
                <?php echo $image['caption']; ?>
              </div>
            </div>
          <?php endforeach; ?>
        </div><!-- end .slider-main -->

    <?php endif; ?>
  </div>

  <?php if( get_field('section_about_us') ): ?>
    <div class="section padding-tbxlarge home-about clearfix" data-aos="fade-up" data-aos-duration="1000">
      <div class="container">
        <div class="col-md-6 col-sm-6">
          <div class="heading-section margin-bmedium text-left">
            <?php
              if( have_rows('tabout') ):
                while ( have_rows('tabout') ): the_row();
                $taboutheading = get_sub_field('ht_about_heading');
                $taboutsubheading = get_sub_field('ht_about_subheading');
            ?>

            <h2><?php echo $taboutheading ?></h2>
            <p><?php echo $taboutsubheading ?></p>

            <?php
              endwhile;
              else :
              // no rows found
              endif;
            ?>
          </div><!-- end .heading-section -->
          <div class="about-post">
            <?php the_field('about_content'); ?>
            <a href="page-blog-single.html" class="btn btn-basic margin-tbmedium">read more</a>
          </div><!-- end .introduce-post -->
        </div>
        <div class="col-md-6 col-sm-6">
          <ul class="image-intro">
            <li><span><img src="<?php the_field('about_first_image'); ?>" alt="" class="img-responsive"></span></li>
            <li><span><img src="<?php the_field('about_second_image'); ?>" alt="" class="img-responsive"></span></li>
          </ul>
        </div>
      </div><!-- end .container -->
    </div><!-- end .home-about -->
  <?php else: ?>
    <!-- undisplay about us -->
  <?php endif; ?>

  <?php if( get_field('section_our_rooms') ): ?>
    <div class="section padding-tbxlarge csuni" data-aos="fade-up" data-aos-duration="1000">
      <div class="container">
        <div class="heading-section heading-padspace text-center">
          <?php
            if( have_rows('troom') ):
              while ( have_rows('troom') ): the_row();
              $troomheading = get_sub_field('ht_room_heading');
              $troomsubheading = get_sub_field('ht_room_subheading');
          ?>

          <h2><?php echo $troomheading ?></h2>
          <p><?php echo $troomsubheading ?></p>

          <?php
            endwhile;
            else :
            // no rows found
            endif;
          ?>
        </div><!-- end .heading-section -->

        <div class="row">
          <?php
        		// check if the repeater field has rows of data
        		if( have_rows('room_list', 166) ):

        			// loop through the rows of data
        			while ( have_rows('room_list', 166) ) : the_row(); ?>
				        <div class="col-md-4 col-sm-4 col-xs-12">
        					<div class="csunibox csunibox__ro">
        						<div class="csunipic csunipic--ro">
        							<span>
        								<img src="<?php the_sub_field('rl_image'); ?>" class="img-responsive">
        							</span>
        						</div>
                    <div class="csoverlay">
                      <div class="csovertitle">
                        <h4><a href="<?php the_sub_field('rl_link'); ?>"><?php the_sub_field('rl_name'); ?></a></h4>
                      </div>
                    </div>
        					</div><!-- end .csunibox -->
        				</div>
              <?php
                endwhile;
                else :
                // no rows found
                endif;
              ?>
  			</div>
      </div><!-- end .container -->
    </div><!-- end .csuni -->
  <?php else: ?>
    <!-- undisplay rooms -->
  <?php endif; ?>

  <?php if( get_field('section_intro') ): ?>
    <div class="section padding-tbxlarge home-why" data-aos="fade-up" data-aos-duration="1000">
      <div class="container">
        <div class="heading-section heading-padspace text-center">
          <?php
            if( have_rows('twhy') ):
              while ( have_rows('twhy') ): the_row();
              $twhyheading = get_sub_field('ht_why_heading');
              $twhysubheading = get_sub_field('ht_why_subheading');
          ?>

          <h2><?php echo $twhyheading ?></h2>
          <p><?php echo $twhysubheading ?></p>

          <?php
            endwhile;
            else :
            // no rows found
            endif;
          ?>
        </div><!-- end .heading-section -->
        <div class="row">
          <?php
        		// check if the repeater field has rows of data
        		if( have_rows('homepage_intro') ):

        			// loop through the rows of data
        			while ( have_rows('homepage_intro') ) : the_row(); ?>

              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="box-content why-post text-center">
                  <div class="box-info">
                    <span class="icon"><i class="ti-layout-accordion-list"></i></span>
                    <h3><?php the_sub_field('intro_title'); ?></h3>
                    <?php the_sub_field('intro_content'); ?>
                  </div><!-- end .box-info -->
                </div><!-- end .box-content -->
              </div>

            <?php
              endwhile;
              else :
              // no rows found
              endif;
            ?>
        </div>
      </div><!-- end .container -->
    </div><!-- end .home-why -->
  <?php else: ?>
    <!-- undisplay intro -->
  <?php endif; ?>

  <?php if( get_field('section_service_facilities') ): ?>
    <section class="section vice padding-tbxlarge csuni clearfix" data-aos="fade-up" data-aos-duration="1000">

      <div class="container">
        <div class="heading-section heading-padspace text-center">
          <?php
            if( have_rows('tservice') ):
              while ( have_rows('tservice') ): the_row();
              $tserviceheading = get_sub_field('ht_service_heading');
              $tservicesubheading = get_sub_field('ht_service_subheading');
          ?>

          <h2><?php echo $tserviceheading ?></h2>
          <p><?php echo $tservicesubheading ?></p>

          <?php
            endwhile;
            else :
            // no rows found
            endif;
          ?>
        </div><!-- end .heading-section -->
        <div class="vboxwrapper">
          <?php
        		// check if the repeater field has rows of data
        		if( have_rows('sf_content', 233) ):

        			// loop through the rows of data
        			while ( have_rows('sf_content', 233) ) : the_row(); ?>
                <div class="vbox">
                  <div class="csunibox csunibox__se">
                    <div class="csunipic csunipic--se">
                      <span>
                        <img src="<?php the_sub_field('sf_image'); ?>" alt="">
                      </span>
                    </div>
                    <div class="csoverlay">
                      <div class="csovertitle">
                        <h4><a href="<?php the_sub_field('sf_title'); ?>"><?php the_sub_field('sf_title'); ?></a></h4>
                      </div>
                    </div>
                  </div><!-- end .csunibox -->
                </div>

              <?php
                endwhile;
                else :
                // no rows found
                endif;
              ?>
        </div>
      </div>
    </section>
  <?php else: ?>
    <!-- undisplay service -->
  <?php endif; ?>

  <?php if( get_field('section_excerpt') ): ?>
    <div class="section padding-tbxlarge home-introduce clearfix">
      <div class="col-md-6 col-sm-6">
        <div class="intro-image">
          <div style="background-image: url(<?php the_field('ex_main_image'); ?>)" class="introduce-img"></div>
        </div><!-- end .intro-image -->
      </div>
      <div class="col-md-6 col-sm-6">
        <div class="introduce-post">
          <h3 class="margin-bmedium">Create Your Own Genuine Masterpiece Recipes</h3>
          <?php the_field('ex_description'); ?>
        </div><!-- end .introduce-post -->
      </div>
    </div><!-- end .home-introduce -->
  <?php else: ?>
    <!-- undisplay exceprt -->
  <?php endif; ?>

  <?php if( get_field('section_testimonial') ): ?>
    <div class="section padding-tbxlarge home-testimonial" data-aos="fade-up" data-aos-duration="1000">
      <div class="container">
        <div class="heading-section heading-padspace text-center">
          <?php
            if( have_rows('gtestimonial') ):
              while ( have_rows('gtestimonial') ): the_row();
              $testiheading = get_sub_field('ht_testimonial_heading');
              $testisubheading = get_sub_field('ht_testimonial_subheading');
          ?>

          <h2><?php echo $testiheading ?></h2>
          <p><?php echo $testisubheading ?></p>

          <?php
            endwhile;
            else :
            // no rows found
            endif;
          ?>
        </div><!-- end .heading-section -->

        <div id="slider-testimonial" class="owl-carousel">
          <?php
            // check if the repeater field has rows of data
            if( have_rows('testimonial_items', 'options') ):

              // loop through the rows of data
              while ( have_rows('testimonial_items', 'options') ) : the_row(); ?>
                <div class="testimo">
                  <div class="testimo-content">
                    <?php the_sub_field('testimonial_content'); ?>
                    <div class="testimo-arrow"></div>
                  </div>
                  <div class="img-testimo">
                    <img width="200" height="200" src="<?php the_sub_field('testimonial_image'); ?>" class="img-responsive" alt="">
                  </div>
                  <div class="wrapper-info">
                    <div class="name"><?php the_sub_field('testimonial_name'); ?>.</div>
                    <div class="position"><?php the_sub_field('testimonial_job__position'); ?></div>
                    <div class="clearfix"></div>
                  </div>
                </div><!-- end .testimo -->
          <?php
              endwhile;
              else :
                // no rows found
            endif;
          ?>
        </div><!-- end .container -->
      </div><!-- end .container -->
    </div><!-- end .home-testimonial -->
  <?php else: ?>
    <!-- undisplay testimonial -->
  <?php endif; ?>

  <?php if( get_field('section_blog') ): ?>
    <div class="section padding-tbxlarge home-blog" data-aos="fade-up" data-aos-duration="1000">
      <div class="container">
        <div class="heading-section heading-padspace text-center">
          <?php
            if( have_rows('tblog') ):
              while ( have_rows('tblog') ): the_row();
              $tblogheading = get_sub_field('ht_blog_heading');
              $tblogsubheading = get_sub_field('ht_blog_subheading');
          ?>

          <h2><?php echo $tblogheading ?></h2>
          <p><?php echo $tblogsubheading ?></p>

          <?php
            endwhile;
            else :
            // no rows found
            endif;
          ?>
        </div><!-- end .heading-section -->
        <div class="row">
          <?php
            $post_objects = get_field('home_content_blog');

            if( $post_objects ): ?>

            <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
              <?php setup_postdata($post); ?>
              <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="box-content blog-post">
                  <a href="<?php the_permalink(); ?>">
                    <div class="box-image">
                      <span>
                        <?php
                          if ( has_post_thumbnail() ) {
                            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                            echo '<img src="'.$image[0].'" data-id="'.$post->ID.'" class="img-responsive">';
                          }
                        ?>
                      </span>
                    </div><!-- end .box-image -->
                    <div class="box-info">
                      <span class="time"><i class="ti-time" aria-hidden="true"></i> <?php the_author(); ?>, <?php the_time('F jS, Y'); ?></span>
                      <h2><?php the_title(); ?></h2>
                    </div><!-- end .box-info -->
                  </a>
                </div><!-- end .box-content -->
              </div><!-- end .col-md-3 col-sm-4 col-xs-12 -->
            <?php endforeach; ?>
            <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
          <?php endif; ?>
        </div><!-- end .row -->
      </div><!-- end .container -->
    </div><!-- end .home-blog -->
  <?php else: ?>
    <!-- undisplay blog -->
  <?php endif; ?>

  <div class="box-subscribe margin-topxlarge">
    <div class="container">

    </div><!-- end .container -->
  </div><!-- end .box-subscribe -->
</div><!-- end #content -->

<script type="text/javascript" charset="utf-8">
  jQuery(window).load(function() {
    jQuery('.flexslider').flexslider({
      controlNav: false
    });
  });
</script>

<?php get_footer(); ?>
