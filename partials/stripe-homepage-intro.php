<div class="block block__intro" data-aos="zoom-in">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-6 custom--col">
        <div class="intro--img"  >
          <?php $images = get_field('home_intro_image'); ?>
              <img src="<?php echo $images['url']; ?>" alt="">
        </div>
      </div>
      <div class="col-md-6 custom--col">
        <div class="intro--desc">
          <h2>Tape Ketan Muntilan</h2>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
            It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
          <button type="submit" class="btn-basic ">See Map</button>
        </div>
      </div>
    </div>
  </div>
</div>
