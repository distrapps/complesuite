<?php
	$image = get_field('footer_background', 'option');
	if( !empty($image) ): ?>

<footer class="main-footer" style="background-image: url('<?php echo $image['url']; ?>')">
	<?php endif; ?>

  <div class="footer-inner">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="footer-logo marbot-medium">
            <?php
              $image = get_field('footer_logo', 'option');
              if( !empty($image) ): ?>
                <a class="" href="index.html">
                  <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="footer-logo img-responsive" />
                </a>
            <?php endif; ?>
          </div><!-- end .footer-logo -->
          <div class="footer-box">
						<?php the_field('footer_note', 'option'); ?>

						<div class="social">
							<?php
								// check if the repeater field has rows of data
								if( have_rows('footer_social', 'option') ):

									// loop through the rows of data
									while ( have_rows('footer_social', 'option') ) : the_row();

										$socialname = get_sub_field('footer_social_name', 'option');
										$sociallink = get_sub_field('footer_social_link', 'option');

										echo '<a href="' . $sociallink . '" class="link share-' . $socialname . '" target="_blank">';
										echo '<i class="ti-' . $socialname . '"></i>';
										echo '</a>';

									endwhile;
									else :
										// no rows found
								endif;
							?>
            </div>
          </div><!-- end .footer-box -->
        </div><!-- end .col-md-4 col-sm-4 col-xs-12 -->

        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="footer-box address">
            <div class="heading-section marbot-medium">
              <h4>Location</h4>
            </div><!-- end .heading-section -->

            <div class="item">
              <i class="ti-location-pin"></i>
              <div class="contact-info">
                <div class="text"><?php the_field('footer_address', 'option'); ?></div>
              </div>
            </div>

						<?php
							// check if the repeater field has rows of data
							if( have_rows('footer_contact', 'option') ):

								// loop through the rows of data
								while ( have_rows('footer_contact', 'option') ) : the_row();

									$contactname = get_sub_field('footer_contact_item', 'option');
									$contactvalue = get_sub_field('footer_contact_value', 'option');

									echo '<div class="item">';
									echo '<i class="ti-' . $contactname . '"></i>';
									echo '<div class="contact-info">';
									echo '<div class="text">'. $contactvalue .'</div>';
									echo '</div>';
									echo '</div>';


								endwhile;
								else :
									// no rows found
							endif;
						?>

          </div><!-- end .footer-box -->
        </div><!-- end .col-md-4 col-sm-4 col-xs-12 -->

        <div class="col-md-4 col-sm-4 col-xs-12">
          <div class="footer-box recent">
            <div class="heading-section marbot-medium">
              <h4>Recent Post</h4>
            </div><!-- end .heading-section -->
            <ul>
                <?php
                $post_objects = get_field('content_blog', 73);

                if( $post_objects ): ?>
                <?php foreach( $post_objects as $post): // variable must be called $post (IMPORTANT) ?>
                <?php setup_postdata($post); ?>

      					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

              <?php endforeach; ?>
              <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
              <?php endif; ?>
            </ul>

          </div><!-- end .footer-box -->
        </div><!-- end .col-md-4 col-sm-4 col-xs-12 -->
      </div><!-- end .row -->
    </div><!-- end .container -->
  </div>

  <div class="copyright">
    <div class="textwidget"><?php the_field('footer_copyright', 'option'); ?></div>
  </div><!-- end .copyright -->
</footer>

<?php wp_footer(); ?>

</body>
</html>
