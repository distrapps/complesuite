<?php
	$image = get_field('footer_background', 'option');
	if( !empty($image) ): ?>

<footer class="main-footer" style="background-image: url('<?php echo $image['url']; ?>')">
	<?php endif; ?>

	<div class="footer-link">
		<div class="container">

			<div class="row">
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="desc-footer">
						<?php
							$image = get_field('footer_logo', 'option');
							if( !empty($image) ): ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="footer-logo img-responsive" />
							<?php endif; ?>

						<?php the_field('footer_note', 'option'); ?>

					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="desc-footer">
						<h3>Address</h3>

						<?php the_field('footer_address', 'option'); ?>

						<ul>
							<?php
								// check if the repeater field has rows of data
								if( have_rows('footer_contact', 'option') ):

									// loop through the rows of data
									while ( have_rows('footer_contact', 'option') ) : the_row();

										$contactname = get_sub_field('footer_contact_item', 'option');
										$contactvalue = get_sub_field('footer_contact_value', 'option');

										echo '<li>'. $contactname .' : '. $contactvalue .'</li>';

									endwhile;
									else :
										// no rows found
								endif;
							?>
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="desc-footer">
						<h3>Quick Link</h3>
						<ul>
							<?php
								// check if the repeater field has rows of data
								if( have_rows('footer_page_link', 'option') ):

									// loop through the rows of data
									while ( have_rows('footer_page_link', 'option') ) : the_row();

										$pagename = get_sub_field('page_link_name', 'option');
										$pagelink = get_sub_field('page_link', 'option');

										echo '<li>';
										echo '<a href="' . $pagelink . '">' . $pagename . '';
										echo '</a>';
										echo '</li>';

									endwhile;
									else :
										// no rows found
								endif;
							?>
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-sm-3 col-xs-12">
					<div class="social-footer text-left">
						<h3>Follow Us</h3>
						<ul>
							<?php
								// check if the repeater field has rows of data
								if( have_rows('footer_social', 'option') ):

									// loop through the rows of data
									while ( have_rows('footer_social', 'option') ) : the_row();

										$socialname = get_sub_field('footer_social_name', 'option');
										$sociallink = get_sub_field('footer_social_link', 'option');

										echo '<li>';
										echo '<a href="' . $sociallink . '" class="icn-' . $socialname . '" target="_blank">';
										echo '<i class="icon-' . $socialname . '"></i>';
										echo '</a>';
										echo '</li>';

									endwhile;
									else :
										// no rows found
								endif;
							?>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- end container -->
	</div>
	<!-- end .footer-content -->

	<div class="footer-info">
		<div class="container">
			<p class="verticent-inner">&copy; 2018 Tape Ketan Muntilan 181. All Rights Reserved.</p>
		</div>
	</div>
	<!-- end .footer-info -->
</footer>

<?php wp_footer(); ?>

</body>

</html>
