<?php

/**
 * Template Name: Rooms
 */

get_header();

?>

<div id="content" class="page">

  <div class="page-intro">
    <?php if( get_field('room_list_cover') ): ?>
      <div class="intro-inner" style="background-image: url('<?php the_field('room_list_cover'); ?>')">
	  <?php endif; ?>
      <div class="outer-inner">
        <div class="inner-box clearfix">
          <div class="inner-box-container">
            <div class="intro-title">
              <h2><?php the_title(); ?> </h2>
              <p>Simply elegance</p>
            </div>
          </div><!-- end .inner-container -->
        </div><!-- end .inner-box -->
      </div><!-- end .outer-inner -->
    </div><!-- end .intro-inner -->
  </div><!-- end .page-intro -->

  <div class="content-wrap">
    <section class="section-introduce">
      <div class="container">
        <div class="heading-section heading-padspace text-center" data-aos="fade-up" data-aos-duration="6000">
          <h2>Our Rooms</h2>
          <p>The Experts Knows Better</p>
        </div><!-- end .heading-section -->

        <div class="row">
          <?php

        		// check if the repeater field has rows of data
        		if( have_rows('room_list') ):

        			// loop through the rows of data
        			while ( have_rows('room_list') ) : the_row(); ?>

              <div class="col-md-4 col-sm-4 col-xs-12" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="50">
                <div class="csunibox csunibox__ro margin-blarge">
                  <div class="csunipic csunipic--ro">
                    <span>
                      <img src="<?php the_sub_field('rl_image'); ?>" class="img-responsive">
                    </span>
                  </div>
                  <div class="csoverlay">
                    <div class="csovertitle">
                      <h4><a href="<?php the_sub_field('rl_link'); ?>"><?php the_sub_field('rl_name'); ?></a></h4>
                    </div>
                  </div>
                </div><!-- end .csunibox -->
              </div>

            <?php
              endwhile;

            else :

              // no rows found

            endif;

          ?>
        </div><!-- end .row -->

      </div><!-- end .container -->
    </section><!-- end .section-introduce -->
  </div><!-- end .content-wrap -->
</div>


<?php get_footer(); ?>
